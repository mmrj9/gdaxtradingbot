import Gdax from 'gdax';
import _ from 'underscore';

const apiURI = 'https://api.gdax.com';
const sandboxURI = 'https://api-public.sandbox.gdax.com';
const key = "";
const b64secret = "";
const passphrase = "";

var Client = require('node-rest-client').Client;

var client = new Client();

let args = {
  headers: {
    "Content-Type": "application/json",
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36"
  }
}

// Defaults to https://api.gdax.com if apiURI omitted
const authedClient = new Gdax.AuthenticatedClient(key, b64secret, passphrase, apiURI);
const publicClient = new Gdax.PublicClient();

const crypto = "ETH";
const initialPrice = 758.51;
const defaultPriceIncrement = 1;
const defaultMargin = 2;
const defaultMinMargin = 0.5;
const defaultMarginDecrement = 0.1;
const defaultProfitPercentage = 0.1;
const addPrice = 1.19;

let lastSalePrice;
let lastBuyPrice;
let minMargin;
let profit = 0;

const process = () => {
  //IF HAS OPEN ORDERS -> DO NOTHING ELSE BUY/SELL
  authedClient.getOrders(function(err, response, data) {
    if (!data) {
      console.log("API ERROR");
      console.log(data);
      return null;
    } else {
      if (data.length === 0) {
        //IF HAS CRYPTO BALANCE -> SELL IF NOT -> BUY
        authedClient.getAccounts(function(err, response, data) {
          let account = _.findWhere(data, {currency: crypto});
          let balance = account.balance;
          let accountId = account.id;

          if (balance > 0) {
            //SELL
            console.log("Sell");
            //GET ORDER BOOK
            client.get(`${apiURI}/products/${crypto}-EUR/book`, args, function(data, response) {
              let bids = data.bids;
              let bestBid = bids[0];
              let bestBidPrice = bestBid[0];
              // let bestBidSize = bestBid[1];
              let unitPrice = bestBidPrice;
              if (!lastBuyPrice) {
                lastBuyPrice = initialPrice;
              }
              if (!minMargin) {
                minMargin = defaultMargin;
              }
              console.log("Unit Price", unitPrice);
              console.log("Last Price + Margin", lastBuyPrice + minMargin);
              console.log("Diff", (lastBuyPrice + minMargin) - unitPrice);
              if (unitPrice >= lastBuyPrice + minMargin) {
                var sellParams = {
                  'price': Number(parseFloat(unitPrice + addPrice).toFixed(2)), // EUR
                  'type': 'limit',
                  'size': Number(parseFloat(balance).toFixed(8)), // BTC
                  'product_id': `${crypto}-EUR`
                };
                console.log("Sell Params", sellParams);
                authedClient.sell(sellParams, function(err, response, data) {
                  if (!err && data.id) {
                    lastSalePrice = unitPrice;
                    minMargin = defaultMargin;
                    profit = profit + (defaultProfitPercentage * balance);
                    console.log("Placed Sell Order", bestBidPrice);
                    console.log("Profit", profit);
                  } else {
                    console.log(data);
                  }
                });
              } else {
                if (minMargin > defaultMinMargin) {
                  minMargin = minMargin - defaultMarginDecrement;
                }
              }
            });
          } else {
            let account = _.findWhere(data, {currency: "EUR"});
            let balance = twoDecimals(account.balance);
            if (balance > 0) {
              //BUY
              console.log("Buy");
              //GET ORDER BOOK
              client.get(`${apiURI}/products/${crypto}-EUR/book`, args, function(data, response) {
                let asks = data.asks;
                let bestAsk = asks[0];
                let bestAskPrice = bestAsk[0];
                // let bestAskSize = bestAsk[1];

                let unitPrice = bestAskPrice;

                if (!lastSalePrice) {
                  lastSalePrice = initialPrice;
                }
                if (!minMargin) {
                  minMargin = defaultMargin;
                }
                console.log("Unit Price", unitPrice)
                console.log("Last Sale Price - Min Margin", lastSalePrice - minMargin)
                console.log("Diff", unitPrice - (lastSalePrice - minMargin))
                if (unitPrice <= lastSalePrice - minMargin) {
                  let size = (balance - 0.03) / (unitPrice - defaultMarginDecrement);
                  var buyParams = {
                    'price': Number((parseFloat(unitPrice)).toFixed(2)), // EUR
                    'type': 'limit',
                    'size': Number((parseFloat(size)).toFixed(8)), // BTC
                    'product_id': `${crypto}-EUR`
                  };
                  console.log(buyParams);
                  authedClient.buy(buyParams, function(err, response, data) {
                    if (!err && data.id) {
                      lastBuyPrice = unitPrice;
                      minMargin = defaultMargin;
                      profit = profit - size;
                      console.log("Placed Buy Order", unitPrice);
                      console.log("Profit", profit);
                    } else {
                      console.log(data);
                    }
                  });
                } else {
                  if (minMargin > defaultMinMargin) {
                    minMargin = minMargin - defaultMarginDecrement;
                  }
                }
              });
            }
          }
        });
      } else {
        console.log("Active order");
      }
    }
  })
  console.log("______________________________");
}

const twoDecimals = (number) => {
  return number.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
}

// client.get(`${apiURI}/products/${crypto}-EUR/book`, args, function(data, response) {   console.log(data); })

process();
setInterval(process, 10000);
