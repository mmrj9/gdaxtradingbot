import Gdax from 'gdax';
import _ from 'underscore';

const apiURI = 'https://api.gdax.com';
const sandboxURI = 'https://api-public.sandbox.gdax.com';
const key = "a40d5f702b0d10c3722af927ad01838f";
const b64secret = "6Xrmjubgnw5yUFUvRvdaVdBlSzjGtPBbRZBa8lBmJULxUjUdQxovW+1EB29vXIbnZY5hlM8i3DvweWwh21NXig==";
const passphrase = "ttjqqga4bgk";

const Client = require('node-rest-client').Client;

const client = new Client();

const args = {
  headers: {
    "Content-Type": "application/json",
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36"
  }
}

// Defaults to https://api.gdax.com if apiURI omitted
const authedClient = new Gdax.AuthenticatedClient(key, b64secret, passphrase, apiURI);
const publicClient = new Gdax.PublicClient();

const crypto = "ETH";
const interval = 30000;
const minOfferDiff = 10;
const minOfferProfit = 0.18;
const forceActionTime = 10080;
const orderCancelTime = 10;
const priceDataMaxElements = 100;
const priceDataMinElements = 5;
const priceDataLastElements = 3;

let startingBalance = 0;
let history = [];
let priceData = [];
let forceAction = false;

const process = () => {
  //IF HAS OPEN ORDERS -> DO NOTHING ELSE BUY/SELL
  console.log("______________________________");
  console.log("History : ", history);
  authedClient.getAccounts(function(err, response, accounts) {
    if (!_.isEmpty(accounts)) {
      let cryptoAccount = _.findWhere(accounts, {currency: crypto});
      let fiatAccount = _.findWhere(accounts, {currency: "EUR"});
      if (!_.isEmpty(cryptoAccount) && !_.isEmpty(fiatAccount)) {
        let cryptoBalance = cryptoAccount.balance;
        let fiatBalance = twoDecimals(fiatAccount.balance);
        if (!startingBalance && fiatBalance > 0 && cryptoBalance <= 0) {
          startingBalance = fiatBalance;
          console.log("Starting Balance :", `${fiatBalance} €`);
        }

        authedClient.getOrders(function(err, response, orders) {
          if (!orders) {
            return null;
          } else {
            if (orders.length === 0) {
              client.get(`${apiURI}/products/${crypto}-EUR/book`, args, function(orderBook, response) {

                let asks = orderBook.asks;
                let bestAsk = asks[0];
                let bestAskPrice = bestAsk[0];

                let bids = orderBook.bids;
                let bestBid = bids[0];
                let bestBidPrice = bestBid[0];

                //Convert crypto to starting balance
                if (!startingBalance) {
                  startingBalance = parseFloat(bestBidPrice * cryptoBalance) + parseFloat(fiatBalance);
                  console.log("Starting Balance :", `${startingBalance} €`);
                }

                if (priceData.length > priceDataMaxElements) {
                  priceData.shift();
                }
                let pData = {
                  price: bestBidPrice,
                  timestamp: new Date()
                }
                priceData.push(pData);

                if (cryptoBalance <= 0) {
                  //BUY
                  buy(bestAskPrice, fiatBalance);
                } else {
                  //SELL
                  sell(bestBidPrice, cryptoBalance);
                }
              })
            } else {
              console.log("Active order");
              if (orders.constructor === Array && !_.isEmpty(orders)) {
                let order = orders.pop();
                let orderTimestamp = new Date(order.created_at);
                let diff = Math.abs(new Date() - orderTimestamp);
                let minutes = Math.floor((diff / 1000) / 60);
                if (minutes >= orderCancelTime) {
                  console.log("Cancelling order,,,");
                  authedClient.cancelOrder(order.id, function(err, response, data) {
                    if (!err) {
                      history.pop();
                      console.log("Order canceled");
                    }
                  });
                }
              }
            }
          }
        });
      }
    }
  });
}

const buy = (bestUnitPrice, fiatBalance) => {
  console.log("Buy");
  let shouldBuy = false;

  if (_.isEmpty(history) || forceAction) {
    shouldBuy = true;
    forceAction = false;
  } else {
    let sells = _.filter(history, function(h) {
      return h.action == "SELL";
    });
    let lastSell = sells.pop();
    if (lastSell) {
      let lastSellUnitPrice = lastSell.unitPrice;

      console.log("Unit Price - minOfferDiff=", parseFloat(parseFloat(bestUnitPrice) - parseFloat(minOfferDiff)).toFixed(2));
      console.log("Last Sell Price =", parseFloat(lastSellUnitPrice).toFixed(2));
      console.log("(Unit Price +  minOfferDiff) - Last Sell Price =", parseFloat(parseFloat(
        parseFloat(bestUnitPrice) + parseFloat(minOfferDiff)
      ) - parseFloat(lastSellUnitPrice)).toFixed(2));
      if (parseFloat(parseFloat(bestUnitPrice) + parseFloat(minOfferDiff)) - parseFloat(lastSellUnitPrice) <= 0) {
        shouldBuy = detectInflection(priceData, true);
      }
    }
  }

  if (shouldBuy) {
    let size = (fiatBalance - 0.05) / (bestUnitPrice - minOfferProfit);
    let buyParams = {
      'price': parseFloat(parseFloat(parseFloat(bestUnitPrice) - parseFloat(minOfferProfit)).toFixed(2)), // EUR
      'type': 'limit',
      'size': parseFloat((size).toFixed(8)), // BTC
      'product_id': `${crypto}-EUR`
    };
    console.log(buyParams);
    authedClient.buy(buyParams, function(err, response, data) {
      if (!err && data.id) {
        history.push({id: data.id, action: "BUY", unitPrice: buyParams.price, size: buyParams.size, timestamp: new Date()});
        priceData = [];
        console.log("Placed Buy Order ");
      } else {
        console.log(data);
      }
    });
  } else {
    console.log("Shouldn't buy");
    let lastAction = history[history.length - 1];
    let lastActionTimestamp = lastAction.timestamp;
    let diff = Math.abs(new Date() - lastActionTimestamp);
    let minutes = Math.floor((diff / 1000) / 60);
    if (minutes >= forceActionTime) {
      forceAction = true;
      console.log("Forcing buy");
    }
  }

}

const sell = (bestUnitPrice, cryptoBalance) => {
  console.log("Sell");
  let shouldSell = false;

  if (_.isEmpty(history) || forceAction) {
    shouldSell = true;
    forceAction = false;
  } else {
    let buys = _.filter(history, function(h) {
      return h.action == "BUY";
    });
    let lastBuy = buys.pop();
    if (lastBuy) {
      let lastBuyUnitPrice = lastBuy.unitPrice;

      console.log("Unit Price + minOfferDiff=", parseFloat(parseFloat(bestUnitPrice) + parseFloat(minOfferDiff)).toFixed(2));
      console.log("Last Buy Price =", parseFloat(lastBuyUnitPrice).toFixed(2));
      console.log(
        "(Unit Price -  minOfferDiff) - Last Buy Price =",
        parseFloat(parseFloat(parseFloat(bestUnitPrice) - parseFloat(minOfferDiff)) - parseFloat(lastBuyUnitPrice)).toFixed(2)
      );
      if ((parseFloat(bestUnitPrice) - parseFloat(minOfferDiff)) - parseFloat(lastBuyUnitPrice) >= 0) {
        shouldSell = detectInflection(priceData, false);
      }
    }
  }

  if (shouldSell) {
    var sellParams = {
      'price': parseFloat((parseFloat(bestUnitPrice) + parseFloat(minOfferProfit)).toFixed(2)), // EUR
      'type': 'limit',
      'size': parseFloat(parseFloat(cryptoBalance).toFixed(8)), // BTC
      'product_id': `${crypto}-EUR`
    };
    console.log(sellParams);
    authedClient.sell(sellParams, function(err, response, data) {
      if (!err && data.id) {
        history.push({id: data.id, action: "SELL", unitPrice: sellParams.price, size: sellParams.size, timestamp: new Date()});
        priceData = [];
        console.log("Placed Sell Order ");
      } else {
        console.log(data);
      }
    });
  } else {
    console.log("Shouldn't sell")
    let lastAction = history[history.length - 1];
    let lastActionTimestamp = lastAction.timestamp;
    let diff = Math.abs(new Date() - lastActionTimestamp);
    let minutes = Math.floor((diff / 1000) / 60);
    if (minutes >= forceActionTime) {
      forceAction = true;
      console.log("Forcing sell");
    }
  }

}

const twoDecimals = (parseFloat) => {
  return parseFloat.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
}

const detectInflection = (priceData, isBuy) => {
  console.log("Price Data : ", priceData);
  if (priceData.length < priceDataMinElements) {
    return true;
  }
  let aux = 0;
  let lastElems = [];
  while (aux < priceDataLastElements) {
    lastElems.push(priceData.pop());
  }
  let lastBeforeInflection = priceData.pop();
  let isInflection = true;
  if (isBuy) {
    lastElems.forEach((el) => {
      if (el.price <= lastBeforeInflection.price) {
        isInflection = false;
      }
    });
  } else {
    lastElems.forEach((el) => {
      if (el.price >= lastBeforeInflection.price) {
        isInflection = false;
      }
    });
  }

  console.log("Is Inflection", isInflection);
  return isInflection;
}

process();
setInterval(process, interval);
