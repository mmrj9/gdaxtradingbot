'use strict';

var _gdax = require('gdax');

var _gdax2 = _interopRequireDefault(_gdax);

var _underscore = require('underscore');

var _underscore2 = _interopRequireDefault(_underscore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var apiURI = 'https://api.gdax.com';
var sandboxURI = 'https://api-public.sandbox.gdax.com';
var key = "a40d5f702b0d10c3722af927ad01838f";
var b64secret = "6Xrmjubgnw5yUFUvRvdaVdBlSzjGtPBbRZBa8lBmJULxUjUdQxovW+1EB29vXIbnZY5hlM8i3DvweWwh21NXig==";
var passphrase = "ttjqqga4bgk";

var Client = require('node-rest-client').Client;

var client = new Client();

var args = {
  headers: {
    "Content-Type": "application/json",
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36"
  }

  // Defaults to https://api.gdax.com if apiURI omitted
};var authedClient = new _gdax2.default.AuthenticatedClient(key, b64secret, passphrase, apiURI);
var publicClient = new _gdax2.default.PublicClient();

var crypto = "ETH";
var minOfferDiff = 10;
var minOfferProfit = 0.18;
var forceActionTime = 10080;
var orderCancelTime = 10;

var startingBalance = 0;
var history = [];
var forceAction = false;
var profit = 0;

var process = function process() {
  //IF HAS OPEN ORDERS -> DO NOTHING ELSE BUY/SELL
  console.log("______________________________");
  console.log("History : ", history);
  authedClient.getAccounts(function (err, response, accounts) {
    if (!_underscore2.default.isEmpty(accounts)) {
      var cryptoAccount = _underscore2.default.findWhere(accounts, { currency: crypto });
      var fiatAccount = _underscore2.default.findWhere(accounts, { currency: "EUR" });
      if (!_underscore2.default.isEmpty(cryptoAccount) && !_underscore2.default.isEmpty(fiatAccount)) {
        var cryptoBalance = cryptoAccount.balance;
        var fiatBalance = twoDecimals(fiatAccount.balance);
        if (!startingBalance && fiatBalance > 0 && cryptoBalance <= 0) {
          startingBalance = fiatBalance;
          console.log("Starting Balance :", fiatBalance + ' \u20AC');
        }

        authedClient.getOrders(function (err, response, orders) {
          if (!orders) {
            return null;
          } else {
            if (orders.length === 0) {
              client.get(apiURI + '/products/' + crypto + '-EUR/book', args, function (orderBook, response) {

                var asks = orderBook.asks;
                var bestAsk = asks[0];
                var bestAskPrice = bestAsk[0];

                var bids = orderBook.bids;
                var bestBid = bids[0];
                var bestBidPrice = bestBid[0];

                //Convert crypto to starting balance
                if (!startingBalance) {
                  startingBalance = parseFloat(bestBidPrice * cryptoBalance) + parseFloat(fiatBalance);
                  console.log("Starting Balance :", startingBalance + ' \u20AC');
                }

                if (cryptoBalance <= 0) {
                  //BUY
                  buy(bestAskPrice, fiatBalance);
                } else {
                  //SELL
                  sell(bestBidPrice, cryptoBalance);
                }
              });
            } else {
              console.log("Active order");
              if (orders.constructor === Array && !_underscore2.default.isEmpty(orders)) {
                var order = orders.pop();
                var orderTimestamp = new Date(order.created_at);
                var diff = Math.abs(new Date() - orderTimestamp);
                var minutes = Math.floor(diff / 1000 / 60);
                if (minutes >= orderCancelTime) {
                  console.log("Cancelling order,,,");
                  authedClient.cancelOrder(order.id, function (err, response, data) {
                    if (!err) {
                      history.pop();
                      console.log("Order canceled");
                    }
                  });
                }
              }
            }
          }
        });
      }
    }
  });
};

var buy = function buy(bestUnitPrice, fiatBalance) {
  console.log("Buy");
  var shouldBuy = false;

  if (_underscore2.default.isEmpty(history) || forceAction) {
    shouldBuy = true;
    forceAction = false;
  } else {
    var sells = _underscore2.default.filter(history, function (h) {
      return h.action == "SELL";
    });
    var lastSell = sells.pop();
    if (lastSell) {
      var lastSellUnitPrice = lastSell.unitPrice;

      console.log("Unit Price - minOfferDiff=", parseFloat(parseFloat(bestUnitPrice) - parseFloat(minOfferDiff)).toFixed(2));
      console.log("Last Sell Price =", parseFloat(lastSellUnitPrice).toFixed(2));
      console.log("(Unit Price +  minOfferDiff) - Last Sell Price =", parseFloat(parseFloat(parseFloat(bestUnitPrice) + parseFloat(minOfferDiff)) - parseFloat(lastSellUnitPrice)).toFixed(2));
      if (parseFloat(parseFloat(bestUnitPrice) + parseFloat(minOfferDiff)) - parseFloat(lastSellUnitPrice) <= 0) {
        shouldBuy = true;
      }
    }
  }

  if (shouldBuy) {
    var size = (fiatBalance - 0.05) / (bestUnitPrice - minOfferProfit);
    var buyParams = {
      'price': parseFloat(parseFloat(parseFloat(bestUnitPrice) - parseFloat(minOfferProfit)).toFixed(2)), // EUR
      'type': 'limit',
      'size': parseFloat(size.toFixed(8)), // BTC
      'product_id': crypto + '-EUR'
    };
    console.log(buyParams);
    authedClient.buy(buyParams, function (err, response, data) {
      if (!err && data.id) {
        history.push({ id: data.id, action: "BUY", unitPrice: buyParams.price, size: buyParams.size, timestamp: new Date() });
        console.log("Placed Buy Order ");
      } else {
        console.log(data);
      }
    });
  } else {
    console.log("Shouldn't buy");
    var lastAction = history[history.length - 1];
    var lastActionTimestamp = lastAction.timestamp;
    var diff = Math.abs(new Date() - lastActionTimestamp);
    var minutes = Math.floor(diff / 1000 / 60);
    if (minutes >= forceActionTime) {
      forceAction = true;
      console.log("Forcing buy");
    }
  }
};

var sell = function sell(bestUnitPrice, cryptoBalance) {
  console.log("Sell");
  var shouldSell = false;

  if (_underscore2.default.isEmpty(history) || forceAction) {
    shouldSell = true;
    forceAction = false;
  } else {
    var buys = _underscore2.default.filter(history, function (h) {
      return h.action == "BUY";
    });
    var lastBuy = buys.pop();
    if (lastBuy) {
      var lastBuyUnitPrice = lastBuy.unitPrice;

      console.log("Unit Price + minOfferDiff=", parseFloat(parseFloat(bestUnitPrice) + parseFloat(minOfferDiff)).toFixed(2));
      console.log("Last Buy Price =", parseFloat(lastBuyUnitPrice).toFixed(2));
      console.log("(Unit Price -  minOfferDiff) - Last Buy Price =", parseFloat(parseFloat(parseFloat(bestUnitPrice) - parseFloat(minOfferDiff)) - parseFloat(lastBuyUnitPrice)).toFixed(2));
      if (parseFloat(bestUnitPrice) - parseFloat(minOfferDiff) - parseFloat(lastBuyUnitPrice) >= 0) {
        shouldSell = true;
      }
    }
  }

  if (shouldSell) {
    var sellParams = {
      'price': parseFloat((parseFloat(bestUnitPrice) + parseFloat(minOfferProfit)).toFixed(2)), // EUR
      'type': 'limit',
      'size': parseFloat(parseFloat(cryptoBalance).toFixed(8)), // BTC
      'product_id': crypto + '-EUR'
    };
    console.log(sellParams);
    authedClient.sell(sellParams, function (err, response, data) {
      if (!err && data.id) {
        history.push({ id: data.id, action: "SELL", unitPrice: sellParams.price, size: sellParams.size, timestamp: new Date() });
        console.log("Placed Sell Order");
      } else {
        console.log(data);
      }
    });
  } else {
    console.log("Shouldn't sell");
    var lastAction = history[history.length - 1];
    var lastActionTimestamp = lastAction.timestamp;
    var diff = Math.abs(new Date() - lastActionTimestamp);
    var minutes = Math.floor(diff / 1000 / 60);
    if (minutes >= forceActionTime) {
      forceAction = true;
      console.log("Forcing sell");
    }
  }
};

var twoDecimals = function twoDecimals(parseFloat) {
  return parseFloat.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
};

process();
setInterval(process, 60000);