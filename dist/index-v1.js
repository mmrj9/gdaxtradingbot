'use strict';

var _gdax = require('gdax');

var _gdax2 = _interopRequireDefault(_gdax);

var _underscore = require('underscore');

var _underscore2 = _interopRequireDefault(_underscore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var apiURI = 'https://api.gdax.com';
var sandboxURI = 'https://api-public.sandbox.gdax.com';
var key = "a40d5f702b0d10c3722af927ad01838f";
var b64secret = "6Xrmjubgnw5yUFUvRvdaVdBlSzjGtPBbRZBa8lBmJULxUjUdQxovW+1EB29vXIbnZY5hlM8i3DvweWwh21NXig==";
var passphrase = "ttjqqga4bgk";

var Client = require('node-rest-client').Client;

var client = new Client();

var args = {
  headers: {
    "Content-Type": "application/json",
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36"
  }

  // Defaults to https://api.gdax.com if apiURI omitted
};var authedClient = new _gdax2.default.AuthenticatedClient(key, b64secret, passphrase, apiURI);
var publicClient = new _gdax2.default.PublicClient();

var crypto = "ETH";
var initialPrice = 758.51;
var defaultPriceIncrement = 1;
var defaultMargin = 2;
var defaultMinMargin = 0.5;
var defaultMarginDecrement = 0.1;
var defaultProfitPercentage = 0.1;
var addPrice = 1.19;

var lastSalePrice = void 0;
var lastBuyPrice = void 0;
var minMargin = void 0;
var profit = 0;

var process = function process() {
  //IF HAS OPEN ORDERS -> DO NOTHING ELSE BUY/SELL
  authedClient.getOrders(function (err, response, data) {
    if (!data) {
      console.log("API ERROR");
      console.log(data);
      return null;
    } else {
      if (data.length === 0) {
        //IF HAS CRYPTO BALANCE -> SELL IF NOT -> BUY
        authedClient.getAccounts(function (err, response, data) {
          var account = _underscore2.default.findWhere(data, { currency: crypto });
          var balance = account.balance;
          var accountId = account.id;

          if (balance > 0) {
            //SELL
            console.log("Sell");
            //GET ORDER BOOK
            client.get(apiURI + '/products/' + crypto + '-EUR/book', args, function (data, response) {
              var bids = data.bids;
              var bestBid = bids[0];
              var bestBidPrice = bestBid[0];
              // let bestBidSize = bestBid[1];
              var unitPrice = bestBidPrice;
              if (!lastBuyPrice) {
                lastBuyPrice = initialPrice;
              }
              if (!minMargin) {
                minMargin = defaultMargin;
              }
              console.log("Unit Price", unitPrice);
              console.log("Last Price + Margin", lastBuyPrice + minMargin);
              console.log("Diff", lastBuyPrice + minMargin - unitPrice);
              if (unitPrice >= lastBuyPrice + minMargin) {
                var sellParams = {
                  'price': Number(parseFloat(unitPrice + addPrice).toFixed(2)), // EUR
                  'type': 'limit',
                  'size': Number(parseFloat(balance).toFixed(8)), // BTC
                  'product_id': crypto + '-EUR'
                };
                console.log("Sell Params", sellParams);
                authedClient.sell(sellParams, function (err, response, data) {
                  if (!err && data.id) {
                    lastSalePrice = unitPrice;
                    minMargin = defaultMargin;
                    profit = profit + defaultProfitPercentage * balance;
                    console.log("Placed Sell Order", bestBidPrice);
                    console.log("Profit", profit);
                  } else {
                    console.log(data);
                  }
                });
              } else {
                if (minMargin > defaultMinMargin) {
                  minMargin = minMargin - defaultMarginDecrement;
                }
              }
            });
          } else {
            var _account = _underscore2.default.findWhere(data, { currency: "EUR" });
            var _balance = twoDecimals(_account.balance);
            if (_balance > 0) {
              //BUY
              console.log("Buy");
              //GET ORDER BOOK
              client.get(apiURI + '/products/' + crypto + '-EUR/book', args, function (data, response) {
                var asks = data.asks;
                var bestAsk = asks[0];
                var bestAskPrice = bestAsk[0];
                // let bestAskSize = bestAsk[1];

                var unitPrice = bestAskPrice;

                if (!lastSalePrice) {
                  lastSalePrice = initialPrice;
                }
                if (!minMargin) {
                  minMargin = defaultMargin;
                }
                console.log("Unit Price", unitPrice);
                console.log("Last Sale Price - Min Margin", lastSalePrice - minMargin);
                console.log("Diff", unitPrice - (lastSalePrice - minMargin));
                if (unitPrice <= lastSalePrice - minMargin) {
                  var size = (_balance - 0.03) / (unitPrice - defaultMarginDecrement);
                  var buyParams = {
                    'price': Number(parseFloat(unitPrice).toFixed(2)), // EUR
                    'type': 'limit',
                    'size': Number(parseFloat(size).toFixed(8)), // BTC
                    'product_id': crypto + '-EUR'
                  };
                  console.log(buyParams);
                  authedClient.buy(buyParams, function (err, response, data) {
                    if (!err && data.id) {
                      lastBuyPrice = unitPrice;
                      minMargin = defaultMargin;
                      profit = profit - size;
                      console.log("Placed Buy Order", unitPrice);
                      console.log("Profit", profit);
                    } else {
                      console.log(data);
                    }
                  });
                } else {
                  if (minMargin > defaultMinMargin) {
                    minMargin = minMargin - defaultMarginDecrement;
                  }
                }
              });
            }
          }
        });
      } else {
        console.log("Active order");
      }
    }
  });
  console.log("______________________________");
};

var twoDecimals = function twoDecimals(number) {
  return number.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
};

// client.get(`${apiURI}/products/${crypto}-EUR/book`, args, function(data, response) {   console.log(data); })

process();
setInterval(process, 10000);